# create-svelte

## Creating a project

If you're seeing this, you've probably already done this step. Congrats!

```bash
# create a new project in the current directory
pnpm create svelte@latest

# create a new project in db-clone
pnpm create svelte@latest . #inside the db-clone directory
```

## Installing Tailwind

`pnpm dlx svelte-add@latest tailwindcss`

I ran `pnpm install` prior to installing Tailwind but ran it again afterwards 
## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
pnpm dev -- --host=10.10.4.41
```

## Building

To create a production version of your app:

```bash
pnpm run build
```

You can preview the production build with `pnpm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.

## Installing Shadcn Svelte 

`pnpm dlx shadcn-svelte@latest init`

